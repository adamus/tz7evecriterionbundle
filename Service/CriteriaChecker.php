<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Service;


use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Psr\Log\LoggerInterface;

use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveCriterionBundle\Model\CriteriaInterface;
use Tz7\EveCriterionBundle\Model\CriterionInterface;


class CriteriaChecker implements CriteriaCheckerInterface
{
    /**
     * @var ExpressionLanguage
     */
    protected $expression;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->expression = new ExpressionLanguage();
    }

    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @return string
     */
    public function resolveCondition(CharacterInterface $character, CriterionInterface $criterion)
    {
        $corp = $character->getCorporation();
        $condition = $criterion->getCondition();
        $condition = str_replace(
            [
                '%characterID%', '%corporationID%', '%allianceID%',
                '%characterName%', '%corpTicker%', '%allianceTicker%'
            ],
            [
                $character->getId(), $corp->getId(), $corp->getAlliance() ? $corp->getAlliance()->getId() : -1,
                $character->getName(), $corp->getTicker(), $corp->getAlliance() ? $corp->getAlliance()->getShort() : ''
            ],
            $condition
        );

        return $condition;
    }

    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @param string $nickName
     * @return bool|int|mixed|null|string
     */
    public function resolveMatcher(CharacterInterface $character, CriterionInterface $criterion, $nickName = "")
    {
        $matchWith = null;
        $corp = $character->getCorporation();

        switch ($criterion->getComparisonType())
        {
            case CriterionInterface::COMPARE_CHARACTER_ID:
                $matchWith = $character->getId();
                break;
            case CriterionInterface::COMPARE_CHARACTER_NAME:
                $matchWith = $character->getName();
                break;
            case CriterionInterface::COMPARE_CORPORATION_ID:
                $matchWith = $corp->getId();
                break;
            case CriterionInterface::COMPARE_CORPORATION_NAME:
                $matchWith = $corp->getName();
                break;
            case CriterionInterface::COMPARE_ALLIANCE_ID:
                $matchWith = $corp->getAlliance() ? $corp->getAlliance()->getId() : false;
                break;
            case CriterionInterface::COMPARE_ALLIANCE_NAME:
                $matchWith = $corp->getAlliance() ? $corp->getAlliance()->getName() : false;
                break;
            case CriterionInterface::COMPARE_NICK_NAME:
                $matchWith = $nickName;
                break;
            default:
                $this->logger->error(sprintf('%s: Invalid compareType "%s"', __METHOD__, $criterion->getComparisonType()));
                return false;
        }

        $this->logger->debug(sprintf('%s: compare %s %s', __METHOD__, $criterion->getComparisonType(), $matchWith));

        return $matchWith;
    }

    /**
     * @param string $matchWith
     * @param CharacterInterface $character
     * @return array
     */
    public function getContext($matchWith, CharacterInterface $character)
    {
        return [
            'condition' => $matchWith,
            'character' => $character,
            'corporation' => $character->getCorporation()
        ];
    }

    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @param string $nickName
     * @return bool
     */
    public function testCriterion(CharacterInterface $character, CriterionInterface $criterion, $nickName = "")
    {
        $this->logger->debug(sprintf('%s: "%s" for criterion #%d', __METHOD__, $character->getName(), $criterion->getId()));

        $condition = $this->resolveCondition($character, $criterion);
        $matchWith = $this->resolveMatcher($character, $criterion, $nickName);

        return $this->testCondition(
            $criterion->getMatchType(),
            $matchWith,
            $condition,
            $this->getContext($matchWith, $character)
        );
    }

    /**
     * @param string $matchType
     * @param string $matchWith
     * @param string $condition
     * @param array  $context
     * @return bool
     */
    public function testCondition($matchType, $matchWith, $condition, array $context = [])
    {
        switch ($matchType)
        {
            case CriterionInterface::MATCH_EQUAL:
                $this->logger->debug(sprintf('%s: "%s" equals "%s"?', __METHOD__, $matchWith, $condition));
                return $matchWith == $condition;
            case CriterionInterface::NEGATE_EQUAL:
                $this->logger->debug(sprintf('%s: "%s" non-equals "%s"?', __METHOD__, $matchWith, $condition));
                return $matchWith != $condition;
            case CriterionInterface::MATCH_PATTERN:
                $this->logger->debug(sprintf('%s: "%s" matches to "%s"?', __METHOD__, $matchWith, $condition));
                try {
                    return !!preg_match($condition, $matchWith);
                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                    return false;
                }
            case CriterionInterface::NEGATE_PATTERN:
                $this->logger->debug(sprintf('%s: "%s" non-matches to "%s"?', __METHOD__, $matchWith, $condition));
                try {
                    return !preg_match($condition, $matchWith);
                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                    return false;
                }
            case CriterionInterface::MATCH_EXPRESSION:
                try {
                    return !!$this->expression->evaluate($condition, $context);
                } catch (\Exception $ex) {
                    $this->logger->error(sprintf('%s: "%s"', __METHOD__, $ex->getMessage()));
                    return false;
                }
            default:
                $this->logger->error(sprintf('%s: Invalid matchType "%s"', __METHOD__, $matchType));
                return false;
        }
    }

    /**
     * @param CharacterInterface $character
     * @param CriteriaInterface $criteria
     * @param string $nickName
     * @return bool
     */
    public function testCriteria(CharacterInterface $character, CriteriaInterface $criteria, $nickName = "")
    {
        $this->logger->debug(sprintf(
            '%s: "%s" for criteria #%d "%s"',
            __METHOD__, $character->getName(), $criteria->getId(), $criteria->getName()
        ));

        if ($criteria->getCriteria()->count() > 0) {
            foreach ($criteria->getCriteria() as $criterion) {
                if (!$this->testCriterion($character, $criterion, $nickName)) {
                    return false;
                }
            }
            return true;
        } else {
            $this->logger->warning(sprintf('%s: Criteria has no Criterion', __METHOD__));
            return false;
        }
    }

    /**
     * @param CharacterInterface $character
     * @param CriteriaInterface $criteria
     * @param string $nickName
     * @return array
     */
    public function getCriteriaMatch(CharacterInterface $character, CriteriaInterface $criteria, $nickName = "")
    {
        $criterionMatch = [];

        /** @var CriterionInterface $criterion */
        foreach ($criteria->getCriteria() as $criterion)
        {
            $condition = $this->resolveCondition($character, $criterion);
            $matchWith = $this->resolveMatcher($character, $criterion, $nickName);

            $criterionMatch[] = [
                'test' => sprintf(
                    '(%s) "%s" %s "%s"',
                    $criterion->getComparisonType(),
                    $matchWith,
                    $criterion->getMatchType(),
                    $condition
                ),
                'result' => $this->testCriterion($character, $criterion, $nickName) ? 'true' : 'false'
            ];
        }

        return $criterionMatch;
    }
}