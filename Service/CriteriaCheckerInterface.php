<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Service;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveCriterionBundle\Model\CriteriaInterface;
use Tz7\EveCriterionBundle\Model\CriterionInterface;


interface CriteriaCheckerInterface
{
    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @return string
     */
    public function resolveCondition(CharacterInterface $character, CriterionInterface $criterion);

    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @param string $nickName
     * @return bool|int|mixed|null|string
     */
    public function resolveMatcher(CharacterInterface $character, CriterionInterface $criterion, $nickName = "");

    /**
     * @param string $matchWith
     * @param CharacterInterface $character
     * @return array
     */
    public function getContext($matchWith, CharacterInterface $character);

    /**
     * @param CharacterInterface $character
     * @param CriterionInterface $criterion
     * @param string $nickName
     * @return bool
     */
    public function testCriterion(CharacterInterface $character, CriterionInterface $criterion, $nickName = "");

    /**
     * @param string $matchType
     * @param string $matchWith
     * @param string $condition
     * @param array  $context
     * @return bool
     */
    public function testCondition($matchType, $matchWith, $condition, array $context = []);

    /**
     * @param CharacterInterface $character
     * @param CriteriaInterface $criteria
     * @param string $nickName
     * @return bool
     */
    public function testCriteria(CharacterInterface $character, CriteriaInterface $criteria, $nickName = "");

    /**
     * @param CharacterInterface $character
     * @param CriteriaInterface $criteria
     * @param string $nickName
     * @return array
     */
    public function getCriteriaMatch(CharacterInterface $character, CriteriaInterface $criteria, $nickName = "");
}