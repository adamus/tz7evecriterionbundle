<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Test\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


use Tz7\EveCriterionBundle\Service\CriteriaCheckerInterface;
use Tz7\EveCriterionBundle\Model\AbstractCriterion;
use Tz7\EveCriterionBundle\Model\AbstractCriteria;
use Tz7\EveCriterionBundle\Skeleton;


class CriterionTest extends KernelTestCase
{
    public function testCriterion()
    {
        static::bootKernel();

        /** @var CriteriaCheckerInterface $checker */
        $checker = static::$kernel->getContainer()->get('tz7.eve_criterion.criteria_checker');

        $acceptedCharacter = $this->generateAcceptedCharacter();
        $notAcceptedCharacter = $this->generateNotAcceptedCharacter();

        foreach ($this->getTestCriteria()->getCriteria() as $criterion) /** @var AbstractCriterion $criterion */
        {
            $this->assertEquals(true, $checker->testCriterion($acceptedCharacter, $criterion));
            $this->assertEquals(false, $checker->testCriterion($notAcceptedCharacter, $criterion));
        }
    }

    protected function getTestCriteria()
    {
        $criteria = new AbstractCriteria();

        $criteriaTypes = [
            AbstractCriterion::COMPARE_CHARACTER_ID => [AbstractCriterion::MATCH_EQUAL, 1],
            AbstractCriterion::COMPARE_CHARACTER_NAME => [AbstractCriterion::MATCH_PATTERN, '/^Accepted Character$/i'],
            AbstractCriterion::COMPARE_CHARACTER_NAME => [AbstractCriterion::MATCH_EXPRESSION, 'corporation.getName() == "Accepted Corporation"']
        ];

        foreach ($criteriaTypes as $comparison => $cases)
        {
            $criterion = new AbstractCriterion();
            $criterion->setComparisonType($comparison);
            $criterion->setMatchType($cases[0]);
            $criterion->setCondition($cases[1]);
            $criteria->addCriterion($criterion);
        }

        return $criteria;
    }

    protected function generateAcceptedCharacter()
    {
        return $this
            ->buildCharacter(1, 'Accepted Character')
            ->setCorporation(
                $this
                    ->buildCorporation(2, 'Accepted Corporation', 'ACC')
                    ->setAlliance(
                        $this->buildAlliance(3, 'Accepted Alliance', 'ACA')
                    )
            )
        ;
    }

    protected function generateNotAcceptedCharacter()
    {
        return $this
            ->buildCharacter(4, 'Not Accepted Character')
            ->setCorporation(
                $this
                    ->buildCorporation(5, 'Not Accepted Corporation', 'NACC')
                    ->setAlliance(
                        $this->buildAlliance(6, 'Not Accepted Alliance', 'NACA')
                    )
            )
        ;
    }

    protected function buildCharacter($id, $name)
    {
        $character = new Skeleton\CharacterSkeleton();

        return $character
            ->setId($id)
            ->setName($name)
        ;
    }

    protected function buildCorporation($id, $name, $ticker)
    {
        $corporation = new Skeleton\CorporationSkeleton();

        return $corporation
            ->setId($id)
            ->setName($name)
            ->setTicker($ticker)
        ;
    }

    protected function buildAlliance($id, $name, $short)
    {
        $alliance = new Skeleton\AllianceSkeleton();

        return $alliance
            ->setId($id)
            ->setName($name)
            ->setShort($short)
        ;
    }
}