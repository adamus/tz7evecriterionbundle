<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;


class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('tz7_eve_criterion');

        $rootNode
            ->children()
                ->arrayNode('model_map')
                    ->children()
                        ->scalarNode('criterion')->isRequired()->end()
                        ->scalarNode('criteria')->isRequired()->end()
                    ->end()
                ->end()
            ->end()
        ;


        return $treeBuilder;
    }
}
