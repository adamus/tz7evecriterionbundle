<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Skeleton;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;


class CharacterSkeleton implements CharacterInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var CorporationInterface
     */
    protected $corporation;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return CorporationInterface
     */
    public function getCorporation()
    {
        return $this->corporation;
    }

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function setCorporation(CorporationInterface $corporation)
    {
        $this->corporation = $corporation;
        return $this;
    }
}