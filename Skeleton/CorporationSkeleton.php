<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Skeleton;


use Tz7\EveApiBundle\Model\CharacterInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;
use Tz7\EveApiBundle\Model\AllianceInterface;


class CorporationSkeleton implements CorporationInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $ticker;

    /**
     * @var CharacterInterface[]
     */
    protected $members = [];

    /**
     * @var AllianceInterface;
     */
    protected $alliance;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTicker()
    {
        return $this->ticker;
    }

    /**
     * @param string $ticker
     * @return $this
     */
    public function setTicker($ticker)
    {
        $this->ticker = $ticker;
        return $this;
    }

    /**
     * @return CharacterInterface[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function addMember(CharacterInterface $character)
    {
        $this->members[$character->getId()] = $character;
        return $this;
    }

    /**
     * @param CharacterInterface $character
     * @return $this
     */
    public function removeMember(CharacterInterface $character)
    {
        unset($this->members[$character->getId()]);
        return $this;
    }

    /**
     * @return AllianceInterface
     */
    public function getAlliance()
    {
        return $this->alliance;
    }

    /**
     * @param AllianceInterface $alliance
     * @return $this
     */
    public function setAlliance(AllianceInterface $alliance = null)
    {
        $this->alliance = $alliance;
        return $this;
    }
}