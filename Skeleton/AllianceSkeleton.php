<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Skeleton;


use Tz7\EveApiBundle\Model\AllianceInterface;
use Tz7\EveApiBundle\Model\CorporationInterface;


class AllianceSkeleton implements AllianceInterface
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $short;

    /**
     * @var CorporationInterface[]
     */
    protected $members = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param string $short
     * @return $this
     */
    public function setShort($short)
    {
        $this->short = $short;
        return $this;
    }

    /**
     * @return CorporationInterface[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function addMember(CorporationInterface $corporation)
    {
        $this->members[$corporation->getId()] = $corporation;
        return $this;
    }

    /**
     * @param CorporationInterface $corporation
     * @return $this
     */
    public function removeMember(CorporationInterface $corporation)
    {
        unset($this->members[$corporation->getId()]);
        return $this;
    }
}