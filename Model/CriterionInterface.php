<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;


interface CriterionInterface
{
    const COMPARE_CHARACTER_ID = 'characterID';
    const COMPARE_CHARACTER_NAME = 'characterName';
    const COMPARE_CORPORATION_ID = 'corporationID';
    const COMPARE_CORPORATION_NAME = 'corporationName';
    const COMPARE_ALLIANCE_ID = 'allianceID';
    const COMPARE_ALLIANCE_NAME = 'allianceName';
    const COMPARE_NICK_NAME = 'nickName';

    const MATCH_EQUAL = 'equals';
    const NEGATE_EQUAL = 'notEquals';
    const MATCH_PATTERN = 'matching';
    const NEGATE_PATTERN = 'notMatching';
    const MATCH_EXPRESSION = 'isTrue';

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getComparisonType();

    /**
     * @param string $comparisonType
     * @return $this
     */
    public function setComparisonType($comparisonType);

    /**
     * @return string
     */
    public function getCondition();

    /**
     * @param string $condition
     * @return $this
     */
    public function setCondition($condition);

    /**
     * @return string
     */
    public function getMatchType();

    /**
     * @param string $matchType
     * @return $this
     */
    public function setMatchType($matchType);

    /**
     * @return ArrayCollection
     */
    public function getCriteria();

    /**
     * @param CriteriaInterface $criterion
     * @return $this
     */
    public function addCriteria(CriteriaInterface $criterion);

    /**
     * @param CriteriaInterface $criterion
     * @return $this
     */
    public function removeCriteria(CriteriaInterface $criterion);
}