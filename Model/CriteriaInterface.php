<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Model;


use Doctrine\Common\Collections\ArrayCollection;


interface CriteriaInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return ArrayCollection
     */
    public function getCriteria();

    /**
     * @param CriterionInterface $criterion
     * @return $this
     */
    public function addCriterion(CriterionInterface $criterion);

    /**
     * @param CriterionInterface $criterion
     * @return $this
     */
    public function removeCriterion(CriterionInterface $criterion);
}