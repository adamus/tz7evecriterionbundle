<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\MappedSuperclass
 */
class AbstractCriteria implements CriteriaInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=128)
     */
    protected $name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tz7\EveCriterionBundle\Model\CriterionInterface", inversedBy="criteria", cascade={"persist"})
     * @ORM\JoinTable(
     *     name="tz7_criteria_has_criterion",
     *     joinColumns = {
     *         @ORM\JoinColumn(
     *             name="criteria_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     },
     *     inverseJoinColumns = {
     *         @ORM\JoinColumn(
     *             name="criterion_id",
     *             referencedColumnName="id",
     *             onDelete="CASCADE"
     *         )
     *     }
     * ))
     */
    protected $criteria;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean", options={"default": "0"})
     */
    protected $enabled;

    /**
     *
     */
    public function __construct()
    {
        $this->criteria = new ArrayCollection();
        $this->enabled = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param CriterionInterface $criterion
     * @return $this
     */
    public function addCriterion(CriterionInterface $criterion)
    {
        $this->criteria->add($criterion);
        return $this;
    }

    /**
     * @param CriterionInterface $criterion
     * @return $this
     */
    public function removeCriterion(CriterionInterface $criterion)
    {
        $this->criteria->removeElement($criterion);
        return $this;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
}