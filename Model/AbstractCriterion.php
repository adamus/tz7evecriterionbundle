<?php

/*
 * This file is part of the Tz7\EveCriterionBundle package.
 *
 * (c) Adamus TorK <http://github.com/adamus-tork>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Tz7\EveCriterionBundle\Model;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\MappedSuperclass
 */
class AbstractCriterion implements CriterionInterface
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tz7\EveCriterionBundle\Model\CriteriaInterface", mappedBy="criteria")
     */
    protected $criteria;

    /**
     * @var string
     * @ORM\Column(name="comparison_type", type="string", length=16)
     */
    protected $comparisonType;

    /**
     * @var string
     * @ORM\Column(name="condition_value", type="string", length=255)
     */
    protected $condition;

    /**
     * @var string
     * @ORM\Column(name="match_type", type="string", length=16)
     */
    protected $matchType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getComparisonType()
    {
        return $this->comparisonType;
    }

    /**
     * @param string $comparisonType
     * @return $this
     */
    public function setComparisonType($comparisonType)
    {
        $this->comparisonType = $comparisonType;
        return $this;
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     * @return $this
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return string
     */
    public function getMatchType()
    {
        return $this->matchType;
    }

    /**
     * @param string $matchType
     * @return $this
     */
    public function setMatchType($matchType)
    {
        $this->matchType = $matchType;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param CriteriaInterface $criterion
     * @return $this
     */
    public function addCriteria(CriteriaInterface $criterion)
    {
        $this->criteria->add($criterion);
        return $this;
    }

    /**
     * @param CriteriaInterface $criterion
     * @return $this
     */
    public function removeCriteria(CriteriaInterface $criterion)
    {
        $this->criteria->removeElement($criterion);
        return $this;
    }
}