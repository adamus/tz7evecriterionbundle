TZ-7: EveCriterionBundle
======

This Symfony 2 bundle is part of the [TZ-7 project](https://bitbucket.org/adamus/tz-7), 
providing EVE Online criterion matching tools.

[Documentation](Resources/doc/index.md)


## Installation ##

### Requirements ###

* [Tz7\EveApiBundle](https://bitbucket.org/adamus/tz7eveapibundle) for common interfaces

### Composer ###

Extend the repositories in composer.json:
    
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:adamus/tz7eveapibundle.git"
        }
    ],

  
Then run the following command:

    composer require tz7/eve-criterion-bundle:dev-master


Implement the following interfaces (from Tz7\EveCriterionBundle\Model namespace):

* CriteriaInterface
* CriterionInterface


Bundle needs to be registered in the AppKernel:

    new Tz7\EveCriterionBundle\Tz7EveCriterionBundle(),


## Configuration ##

    tz7_eve_criterion:
        model_map:
            criterion: YourBundle\Entity\YourCriterion # implementing Tz7\EveCriterionBundle\Model\CriterionInterface
            criteria: YourBundle\Entity\YourCriteria # implementing Tz7\EveCriterionBundle\Model\CriteriaInterface
