TZ-7: EveCriterionBundle
======


## Main concepts: ##
 
### Reusability: ###

The bundle should work with any Symfony (currently only tested with ~2.7) project.

* Interface to Entity map for Doctrine allows implement the persisted models separately (currently ORM only).
* Services are accessing objects as interfaces. (@TODO interface for services)
* Traits and MappedSuperclasses to shortcut implementation.


## Criteria Checking ##

[Criteria](../../Model/CriteriaInterface.php) is builds up from several [Criterion](../../Model/CriterionInterface.php) 
which can specify requirements.


A Criterion is like 'corporationName equals "HUN Corp."' or 'nickName matching "/\\[HUN\\]\sAdamus TorK"/'

* comparisonType can be characterID, characterName, etc - _what to compare_
* matchType can be equals, notEquals, etc - _how to compare_
* condition is the desired value to match with - _to compare with_


From the 'corporationName equals "HUN Corp."' example:

* comparisonType = "corporationName"
* matchType = "equals"
* condition = "HUN Corp."


For all possible variation see [CriterionInterface](../../Model/CriterionInterface.php) constants
and [CriteriaChecker](../../Service/CriteriaChecker.php) resolve methods.

It is also possible to define dynamic condition, for example if you want force your members to put their corporation 
tickers as prefix to their TeamSpeak3 nick name, then the condition should look like this: "/^\\[%corpTicker%\\]\s%characterName%/"


Examples: [CriterionTest](../../Tests/Service/CriterionTest.php)
